package com.company;

import com.company.objectModels.ObjectModel;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.Arrays;
import java.util.Collections;

public class FluentWaiter {
    private static WebDriver dr;
    public void instanceCreator(){
//        System.setProperty("webdriver.firefox.bin","C:\\Users\\zubalikk\\AppData\\Local\\Mozilla Firefox\\firefox.exe");
//        System.setProperty("webdriver.chrome.driver", "C:\\Users\\zubalikk\\Desktop\\chromedriver.exe");
//        System.setProperty("webdriver.gecko.driver", "C:\\Users\\zubalikk\\Desktop\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", "A:\\chromedriver\\chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--start-maximized");
        chromeOptions.addArguments("--disable-infobars");
        chromeOptions.addArguments("--disable-notifications");
        chromeOptions.setExperimentalOption("useAutomationExtension", false);
        chromeOptions.setExperimentalOption("excludeSwitches", Collections.singletonList("enable-automation"));
//        _driver = new FirefoxDriver();
//        _driver.manage().window().maximize();
        dr = new ChromeDriver(chromeOptions);
        dr.get("https://www.alza.cz/");
    }
    public void endOfTesting(){
        dr.close();
        dr.quit();
    }
    public  WebElement forAllReady(String webElement) {
        long timeout=15;
        long pollingrate=500;
        String elementor = "html/body"+webElement;
        WebElement WebEle = waitForAllthings(pollingrate,timeout,elementor);
        return WebEle;
    }
    public WebElement forAllWithTimeSettings(long MiliSeconds, long WaitTimeout, String webElement) {
        WebElement WebEle = waitForAllthings(MiliSeconds,WaitTimeout,webElement);
        return WebEle;
    }
    private  WebElement fluentWaitForPresenceOfElementLocated(long MiliSeconds, long WaitTimeout, String webElement) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(dr)
                .withTimeout(Duration.ofSeconds(WaitTimeout))
                .pollingEvery(Duration.ofMillis(MiliSeconds))
                .ignoring(NoSuchElementException.class);
        WebElement element =wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(webElement)));
        return element;
    }
    private WebElement fluentWaitForElementToClickable(long MiliSeconds, long WaitTimeout, String webElement) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(dr)
                .withTimeout(Duration.ofSeconds(WaitTimeout))
                .pollingEvery(Duration.ofMillis(MiliSeconds))
                .ignoring(NoSuchElementException.class);
        WebElement element;
        element=wait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElement)));
        return element;
    }
    private WebElement waitForAllthings(long miliseconds, long secons, String webElement){
        FluentWait<WebDriver> wait= new FluentWait<>(dr)
                .withTimeout(Duration.ofSeconds(secons))
                .pollingEvery(Duration.ofMillis(miliseconds))
                .ignoreAll(Arrays.asList(
                        org.openqa.selenium.NoSuchElementException.class,
                        StaleElementReferenceException.class,
                        ElementNotInteractableException.class
                ));
        wait.until(ExpectedConditions.jsReturnsValue("return document.readyState=='complete';"));
        wait.until((ExpectedConditions.jsReturnsValue("return $.active==0;")));
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(webElement)));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(webElement)));
        WebElement element=wait.until(ExpectedConditions.elementToBeClickable(By.xpath(webElement)));
        return element;
    }
    public  void waitForInvisible(String webElement) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(dr)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofMillis(500))
                .ignoring(NoSuchElementException.class);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(webElement)));
    }
    private void clickOnNoMoveElement(String element){
        int b = 1;
        while (b==1){
            try {
                Rectangle res = forAllReady(element).getRect();
                int x1 = res.getX();
                int y1 = res.getY();
//                System.out.println(res.getX());
//                System.out.println(res.getY());
                Thread.sleep(500);
                Rectangle rez = forAllReady(element).getRect();
                int x2 = rez.getX();
                int y2 = rez.getY();
//                System.out.println(rez.getX());
//                System.out.println(rez.getY());
                if ((x1 == x2) && (y1 == y2)) {
                    System.out.println("Lokace je Stejná");
                    b += 1;
                } else {
                    System.out.println("Lokace není stejná :(");
                    Thread.sleep(500);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public void clickOnElement(String element){
        forAllReady(element);
        WebElement WebEle=dr.findElement(By.xpath(element));
        WebEle.click();
    }
    public void sendKeysToElement(String element, String keysToInput){
        forAllReady(element);
        WebElement trytoclick=dr.findElement(By.xpath(element));
        trytoclick.sendKeys(keysToInput);
    }
    public String textFromElement(String elementToRead){
        String validationText= forAllReady(elementToRead).getText();
        return validationText;
    }
}
