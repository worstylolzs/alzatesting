package com.company.objectModels.Cart;

import com.company.FluentWaiter;
import org.openqa.selenium.WebElement;

public class BeforeCart extends FluentWaiter {
    public String continuesToTheCartXpath(){
        return "//*[contains(text(),'Pokračovat do košíku')]";
    }
    public WebElement continuesToTheCartWebEle(){
        return forAllReady(continuesToTheCartXpath());
    }
}
