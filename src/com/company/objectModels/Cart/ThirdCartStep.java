package com.company.objectModels.Cart;

import com.company.FluentWaiter;
import org.openqa.selenium.WebElement;

public class ThirdCartStep extends FluentWaiter {
    public String emailInputxPath() {
        return "//*[@id='userEmail']";
    }

    public WebElement emailInputWebEle() {
        return forAllReady(emailInputxPath());
    }

    public String phoneInputxPath() {
        return "//*[@id='inpTelNumber']";
    }

    public WebElement phoneInputWebEle() {
        return forAllReady(phoneInputxPath());
    }

    public String billingInformationsCheckboxXpath() {
        return "//*[@for='questionBillingInformation']";
    }

    public WebElement billingInformationsCheckboxWebEle() {
        return forAllReady(billingInformationsCheckboxXpath());
    }

    public String nameInputxPath() {
        return "//*[@id='name']";
    }

    public WebElement nameInputWebEle() {
        return forAllReady(nameInputxPath());
    }

    public String streetInputxPath() {
        return "//*[@id='street']";
    }

    public WebElement streetInputWebEle() {
        return forAllReady(streetInputxPath());
    }

    public String cityInputxPath() {
        return "//*[@id='city']";
    }

    public WebElement cityInputWebEle() {
        return forAllReady(cityInputxPath());
    }

    public String zipCodeInputxPath() {
        return "//*[@id='zip']";
    }

    public WebElement zipCodeInputWebEle() {
        return forAllReady(zipCodeInputxPath());
    }

    public String vatParamIcoInputxPath() {
        return "//*[@id='vatParamIco']";
    }

    public WebElement vatParamIcoInputWebEle() {
        return forAllReady(vatParamIcoInputxPath());
    }

    public String vatParamIcdphInputxPath() {
        return "//*[@id='vatParamIcdph']";
    }

    public WebElement vatParamIcdphInputWebEle() {
        return forAllReady(vatParamIcdphInputxPath());
    }

    public String accountNumberInputxPath() {
        return "//*[@id='accountNumber']";
    }

    public WebElement accountNumberInputWebEle() {
        return forAllReady(accountNumberInputxPath());
    }

    public String bankCodeInputxPath() {
        return "//*[@id='bankCode']";
    }

    public WebElement bankCodeInputWebEle() {
        return forAllReady(bankCodeInputxPath());
    }

    public String specificSymbolInputxPath() {
        return "//*[@id='specificSymbol']";
    }

    public WebElement specificSymbolInputWebEle() {
        return forAllReady(specificSymbolInputxPath());
    }

    public String internalOrderNumberInputxPath() {
        return "//*[@id='internalOrderNumber']";
    }

    public WebElement internalOrderNumberInputWebEle() {
        return forAllReady(internalOrderNumberInputxPath());
    }

    public String questionRememberUserDataCheckBoxxPath() {
        return "//*[@for='questionRememberUserData']";
    }

    public WebElement questionRememberUserDataCheckBoxWebEle() {
        return forAllReady(questionRememberUserDataCheckBoxxPath());
    }

    public String passwordInputxPath() {
        return "//*[@id='password']";
    }

    public WebElement passwordInputWebEle() {
        return forAllReady(passwordInputxPath());
    }

    public String passwordAgainxPath() {
        return "//*[@id='passwordAgain']";
    }

    public WebElement passwordAgainWebEle() {
        return forAllReady(passwordAgainxPath());
    }

}
