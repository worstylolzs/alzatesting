package com.company.objectModels.Cart;

import com.company.FluentWaiter;
import org.openqa.selenium.WebElement;

public class SecontCartStep extends FluentWaiter {
    public String personalCollectionHolesovicexPath() {
        return "//*[@data-deliveryid='595']/self::div";
    }

    public WebElement personalCollectionHolesoviceWebEle() {
        return forAllReady(personalCollectionHolesovicexPath());
    }

    public String personalCollectionHolesovicePopUpNextButtonxPath() {
        return "//*[@id='personalCentralDialog']//a[2]";
    }

    public WebElement personalCollectionHolesovicePopUpNextButtonWebEle() {
        return forAllReady(personalCollectionHolesovicePopUpNextButtonxPath());
    }

    public String showAllPaymentsxPath() {
        return "//*[@id='showAllPayments']";
    }

    public WebElement showAllPaymentsWebEle() {
        return forAllReady(showAllPaymentsxPath());
    }

    public String cashPayxPath() {
        return "//*[@data-paymentid='101']/self::div";
    }

    public WebElement cashPayWebEle() {
        return forAllReady(cashPayxPath());
    }

    public String nextButtonxPath() {
        return "//*[@id='order2ButtonsContainer']//*[@id='confirmOrder2Button']";
    }

    public WebElement nextButtonWebEle() {
        return forAllReady(nextButtonxPath());
    }
}
