package com.company.objectModels.Cart;

import com.company.FluentWaiter;
import org.openqa.selenium.WebElement;

public class FirstCartStep extends FluentWaiter {
    public String nextStepxPath() {
        return "//*[@id='blockBtnRight']/a";
    }

    public WebElement nextStepWebEle() {
        return forAllReady(nextStepxPath());
    }

    public String popUpDontForgetxPath() {
        return "//*[@class='btnx normal grey js-close-button close-button']";
    }

    public WebElement popUpDontForgetWebEle() {
        return forAllReady(popUpDontForgetxPath());
    }

    public String crossOnProductxPath() {
        return "//*[@title='Odstranit']";
    }

    public WebElement crossOnProductWebEle() {
        return forAllReady(crossOnProductxPath());
    }

    public String limitationsBlockxPath() {
        return "//*[@id='blockLimitations']";
    }

    public WebElement limitationsBlockWebEle() {
        return forAllReady(limitationsBlockxPath());
    }
}
