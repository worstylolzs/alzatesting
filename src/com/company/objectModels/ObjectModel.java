package com.company.objectModels;

import com.company.FluentWaiter;
import com.company.objectModels.Cart.BeforeCart;
import com.company.objectModels.Cart.FirstCartStep;
import com.company.objectModels.Cart.SecontCartStep;
import com.company.objectModels.Cart.ThirdCartStep;
import com.company.objectModels.Categories.InsideCategory;
import com.company.objectModels.Homepage.Basket;
import com.company.objectModels.Homepage.LeftMenu;
import com.company.objectModels.Product.ProductDetail;
import org.openqa.selenium.WebElement;

public class ObjectModel extends FluentWaiter { // EXTENDOVÁNÍ Fluentwaitu a jeho následné použití.

    public LeftMenu hLM = new LeftMenu(); //>> vytvořené Instance objectových tříd
    public InsideCategory iM=new InsideCategory();
    public ProductDetail pD=new ProductDetail();
    public BeforeCart bC=new BeforeCart();
    public FirstCartStep fCS=new FirstCartStep();
    public SecontCartStep sCS=new SecontCartStep();
    public ThirdCartStep tCS=new ThirdCartStep();
    public Basket hBasket =new Basket();

        //Tyto tyto instance pak mohu používat klasicky přes iM.element.(klasické přikazy co element má např.click)


public class categories extends FluentWaiter { //třída ve třídě Objectmodel
    public class insideCategories extends FluentWaiter {//třída ve třídě categories
        String imageOfRandomProductXpath() {
            return "//*[@id='boxes']//*[@class='bi js-block-image']";
        }
        WebElement imageOfRandomProductWebEle() {//Tohle chci volat přes categories.insideCategories.ImageOfRandomProductWebEle ale nechce mi to vyhledat ten webele.
            return forAllReady(imageOfRandomProductXpath());
        }
    }
}
}
