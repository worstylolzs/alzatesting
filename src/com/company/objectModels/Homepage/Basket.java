package com.company.objectModels.Homepage;

import com.company.FluentWaiter;
import org.openqa.selenium.WebElement;

public class Basket extends FluentWaiter {
    public String basketxPath() {
        return "//*[@id='basket']";
    }

    public WebElement basketWebEle() {
        return forAllReady(basketxPath());
    }
}
