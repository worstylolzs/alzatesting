package com.company.objectModels.Homepage;

import com.company.FluentWaiter;
import org.openqa.selenium.WebElement;

public class LeftMenu extends FluentWaiter {
    public String categoryVyprodejXpath() {return "//*[@title='Výprodej']";}
    public WebElement categoryVyprodejWeble() {return forAllReady(categoryVyprodejXpath());}
}
