package com.company.objectModels.Product;

import com.company.FluentWaiter;
import org.openqa.selenium.WebElement;

public class ProductDetail extends FluentWaiter {
    public String buyProductButtonXpath() {return "//*[@class='row bottom']";}
    public WebElement buyProductButtonWeble() {return forAllReady(buyProductButtonXpath());}
}
