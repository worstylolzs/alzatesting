package com.company.tests;

import com.company.objectModels.ObjectModel;
import org.testng.annotations.Test;

public class Orders extends ObjectModel {
    @Test
    public void orderInAlza(){
            instanceCreator();
            hLM.categoryVyprodejWeble().click();
            iM.imageOfRandomProductWebEle().click();
            pD.buyProductButtonWeble().click();
            bC.continuesToTheCartWebEle().click();
            fCS.nextStepWebEle().click();
            try{
                    fCS.popUpDontForgetWebEle().click();
            } catch (Exception e) {
                e.printStackTrace();
            }
            sCS.personalCollectionHolesoviceWebEle().click();
            sCS.personalCollectionHolesovicePopUpNextButtonWebEle().click();
            sCS.showAllPaymentsWebEle().click();
            sCS.cashPayWebEle().click();
            sCS.nextButtonWebEle().click();
            tCS.emailInputWebEle().clear();
            tCS.emailInputWebEle().sendKeys("seleniumTestUser@selenium.com");
            tCS.phoneInputWebEle().sendKeys("123456789");
            tCS.billingInformationsCheckboxWebEle().click();
            tCS.nameInputWebEle().sendKeys("seleniumUser");
            tCS.streetInputWebEle().sendKeys("randomStreet");
            tCS.cityInputWebEle().sendKeys("randomCity");
            tCS.zipCodeInputWebEle().sendKeys("0000");
            tCS.vatParamIcoInputWebEle().sendKeys("12345678");
            tCS.vatParamIcdphInputWebEle().sendKeys("CZ12345678");
            tCS.accountNumberInputWebEle().sendKeys("1234567891234567891");
            tCS.bankCodeInputWebEle().sendKeys("1234");
            tCS.specificSymbolInputWebEle().sendKeys("123456789");
            tCS.internalOrderNumberInputWebEle().sendKeys("12345777");
            tCS.questionRememberUserDataCheckBoxWebEle().click();
            tCS.passwordInputWebEle().sendKeys("password123");
            tCS.passwordAgainWebEle().sendKeys("password123");
            hBasket.basketWebEle().click();
            fCS.crossOnProductWebEle().click();
            waitForInvisible(fCS.limitationsBlockxPath());
            endOfTesting();
    }
}
